# Changelog
All notable changes to this project wiil be documented int this file

# [2]
### Added
- El juego Tenis ya tiene implementado el movimiento de raquetas y pelota
- Se ha implementado que se reduzca  el tamaño de la pelota con el tiempo
- Dependiendo de los puntos ya marcados, se reseteará a un radio mayor 
o menor en el momento de marcar punto. 

# [1.5]
### Added
- Introducción de todo el juego tenis, sin tener movimiento la pelota ni raquetas



